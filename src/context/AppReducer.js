export default (state, action) => {
  switch (action.type) {
    case "ADD_ENTRY":
      return {
        ...state, entries: [...state.entries, action.payload ]
      }
    case "DELETE_ENTRY":
      return {
        ...state, entries: state.entries.filter(item => item.id !== action.payload)
       }
    default:
      return state;
  }
}