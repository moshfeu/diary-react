import React, { useContext } from 'react'
import { Container, Row } from 'react-bootstrap'
import { GlobalContext} from '../context/GlobalContext'
import Entry from '../components/Entry'
import { Link } from 'react-router-dom'

// display a list of entries here
const Home = () => {

  const { entries} = useContext(GlobalContext)

  if (!entries) return <Row className="no-entries">
              <h2>No Diary entries so far </h2>
              <Link to="/add" className="no-entries-btn">Add some </Link>
            </Row>

  return (
    <div>
      <Container>

        { entries.length &&
            <>
              <Row className="py-4"><h2 className="ml-auto mr-auto">Recent entries</h2></Row>
              <Row className="py-4 entry-wrapper">
                {entries.map((entry, index) => (
                  <Entry key={index} entry={entry} />
                ))}
              </Row>
            </>
          }
      </Container>
    </div>
  )
}

export default Home